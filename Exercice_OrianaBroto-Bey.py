# analytical solution

import pandas as pd
import numpy as np
import scipy.stats as ss
import time

def d1(S0, K, r, q, sigma, T):
    return ((np.log(S0)/K) + (r - q + (sigma**2)/2) * T) / (sigma * np.sqrt(T))
print(d1(100, 90, 0.01, 0.02, 0.1, 1))

def d2(S0, K, r, q, sigma, T):
    return ((np.log(S0)/K) + (r - q - (sigma**2)/2) * T) / (sigma * np.sqrt(T))
print(d2(100, 90, 0.01, 0.02, 0.1, 1))

def BlackScholesFunction(S0, K, r, T):
    return (S0 * ss.norm.cdf(d1(100, 90, 0.01, 0.02, 0.1, 1)) - K * np.exp(-r * T) * ss.norm.cdf(d2(100, 90, 0.01, 0.02, 0.1, 1)))
print(BlackScholesFunction(100, 90, 0.01, 1))


